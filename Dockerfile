FROM python:3.8
WORKDIR /srv
COPY . .
RUN pip install --upgrade pip
ADD requirements.txt /srv/
RUN pip install -r requirements.txt
ADD . /srv/
CMD python manage.py makemigrations
CMD python manage.py migrate
CMD python manage.py loaddata /fixtures/init_data.json
CMD python manage.py runserver 0.0.0.0:8000

