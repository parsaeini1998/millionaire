from django.urls import path

from game.views.answer_question_apiview import AnswerQuestionAPIView
from game.views.home_templateview import HomeTemplateView
from game.views.initial_game_apiview import InitialGameAPIView
from game.views.top_players_retreive_view import TopPlayersListAPIView
from game.views.game_initial_templateview import GameInitialTemplateView

app_name = "game"

urlpatterns = [
    path('answer/<str:game_uuid>/', AnswerQuestionAPIView.as_view(), name="answer"),
    path('initial/', InitialGameAPIView.as_view(), name="initial"),
    path('top-players/', TopPlayersListAPIView.as_view(), name="top-players"),

    path('home/', HomeTemplateView.as_view(), name="home"),
    path('game-initial/', GameInitialTemplateView.as_view(), name="initial-template"),
]
