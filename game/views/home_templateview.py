from utils.views import BaseTemplateView


class HomeTemplateView(BaseTemplateView):
    template_name = "game/home.html"
