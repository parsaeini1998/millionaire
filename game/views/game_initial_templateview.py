from utils.views import BaseTemplateView


class GameInitialTemplateView(BaseTemplateView):
    template_name = "game/game-initial.html"
