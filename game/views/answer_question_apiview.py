from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from game.models import Game
from game.serializers import AnswerQuestionSerializer


class AnswerQuestionAPIView(APIView):
    permission_classes = [IsAuthenticated]

    def post(self, request, *args, **kwargs):
        serializer = AnswerQuestionSerializer(
            data=request.data, context={
                "game": Game.objects.filter(uuid=self.kwargs.get("game_uuid")).last()
            }
        )
        if serializer.is_valid(raise_exception=True):
            serializer.save()
            return Response(data={"data": serializer.data}, status=200)
        return Response(data=serializer.data, status=400)