from rest_framework.generics import ListAPIView
from rest_framework.response import Response

from game.models import Game
from game.serializers import TopPlayerSerializer


class TopPlayersListAPIView(ListAPIView):
    serializer_class = TopPlayerSerializer

    def get_queryset(self):
        return Game.objects.all().order_by("user_id", "-total_score").distinct("user_id")[:5]

    def list(self, request, *args, **kwargs):
        response = super(TopPlayersListAPIView, self).list(request, *args, **kwargs)
        return Response(data={"data": response.data})
