from django.contrib.auth.models import User
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from game.models import Game, Question
from game.serializers import GameSerializer


class InitialGameAPIView(APIView):
    permission_classes = [IsAuthenticated]

    def post(self, request):
        questions = (
            Question.objects.order_by("?")[:5]
        )

        game = Game.objects.create(
            user=User.objects.get(username="parsa"),
        )
        for question in questions:
            game.questions.add(question)

        serializer = GameSerializer(instance=game)
        return Response(
            data={"data": serializer.data}
        )
