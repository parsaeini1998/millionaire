from django.contrib import admin

# Register your models here.
from game.models import Question, Option, Game

admin.site.register(Game)
admin.site.register(Option)


class OptionInlineAdmin(admin.StackedInline):
    model = Option
    extra = 1


@admin.register(Question)
class QuestionAdmin(admin.ModelAdmin):
    list_display = ("text", "score")
    inlines = [OptionInlineAdmin, ]

