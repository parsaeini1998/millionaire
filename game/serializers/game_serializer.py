from rest_framework import serializers

from game.models import Game
from game.serializers import QuestionSerializer


class GameSerializer(serializers.ModelSerializer):
    questions = QuestionSerializer(many=True)

    class Meta:
        model = Game
        fields = ("total_score", "questions", "uuid")
