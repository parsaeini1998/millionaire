from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from game.models import Question, Option


class AnswerQuestionSerializer(serializers.Serializer):
    question = serializers.IntegerField(required=True)
    option = serializers.IntegerField(required=True)

    def validate(self, attrs):
        question = Question.objects.filter(id=attrs.get("question")).last()
        if not question:
            raise ValidationError("Question does not exist", code="no_question")

        option = Option.objects.filter(id=attrs.get("option")).last()
        if not option:
            raise ValidationError("Option does not exist", code="no_option")

        if option.id not in question.options.all().values_list("id", flat=True):
            raise ValidationError("Option and question are not related", code="not_related")
        return attrs

    def create(self, validated_data):
        game = self.context.get("game")
        question = Question.objects.get(id=validated_data.get("question"))
        if (
            validated_data.get("option") == question.options.filter(is_correct=True).last().id
        ):
            game.total_score += question.score
            game.save()
        return question

    def to_representation(self, instance):
        return {
            "correct_answer": instance.options.filter(is_correct=True).last().id,
            "game_score": self.context.get("game").total_score,
            "game_uuid": self.context.get("game").uuid
        }
