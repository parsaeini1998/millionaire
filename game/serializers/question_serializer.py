from rest_framework import serializers

from game.models import Question
from game.serializers import OptionSerializer


class QuestionSerializer(serializers.ModelSerializer):
    options = OptionSerializer(many=True)

    class Meta:
        model = Question
        fields = (
            "options",
            "text",
            "score",
            "id"
        )
