from rest_framework import serializers

from game.models import Option


class OptionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Option
        fields = (
            "text",
            "id"
        )
