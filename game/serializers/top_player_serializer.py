from rest_framework import serializers

from game.models import Game
from user.serializers import UserSerializer


class TopPlayerSerializer(serializers.ModelSerializer):
    user = UserSerializer()

    class Meta:
        model = Game
        fields = (
            "user",
            "total_score"
        )
