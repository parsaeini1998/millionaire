from django.core.exceptions import ValidationError
from django.db import models
from django.utils.translation import gettext_lazy as _


class Option(models.Model):
    text = models.TextField(
        verbose_name=_("text"),
        max_length=1024,
    )
    is_correct = models.BooleanField(
        default=False,
        verbose_name=_("is_correct")
    )
    question = models.ForeignKey(
        "game.Question",
        verbose_name=_("question"),
        related_name="options",
        on_delete=models.CASCADE
    )

    class Meta:
        verbose_name = "option"
        verbose_name_plural = "options"

    def __str__(self):
        return f"{self.text[:50]} / {str(self.is_correct)}"

