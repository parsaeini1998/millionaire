from uuid import uuid4

from django.contrib.auth.models import User
from django.utils.translation import gettext_lazy as _

from django.db import models


class Game(models.Model):
    uuid = models.CharField(
        verbose_name=_("uuid"),
        max_length=64,
        default=uuid4
    )
    user = models.ForeignKey(
        User,
        verbose_name=_("user"),
        related_name="games",
        on_delete=models.CASCADE
    )
    questions = models.ManyToManyField(
        "game.Question",
        verbose_name=_("questions"),
        related_name="games"
    )
    total_score = models.PositiveIntegerField(
        verbose_name=_("total_score"),
        default=0
    )

    class Meta:
        verbose_name = "game"
        verbose_name_plural = "games"

    def __str__(self):
        return f"{self.user.username} / {self.uuid}"
