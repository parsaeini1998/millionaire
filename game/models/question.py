from django.core.validators import MinValueValidator, MaxValueValidator
from django.db import models
from django.utils.translation import gettext_lazy as _


class Question(models.Model):
    text = models.TextField(
        verbose_name=_("text"),
        max_length=2056,
    )
    score = models.PositiveIntegerField(
        default=5,
        validators=[MinValueValidator(5), MaxValueValidator(20)]
    )

    class Meta:
        verbose_name = "question"
        verbose_name_plural = "questions"

    def __str__(self):
        return f"{self.text[:50]} / {str(self.score)}"
