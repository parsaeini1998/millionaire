from django.views.generic import TemplateView


class BaseTemplateView(TemplateView):

    def get_context_data(self, **kwargs):
        context = super(BaseTemplateView, self).get_context_data(**kwargs)
        context.update({
            "user": self.request.user,
            "password": self.request.COOKIES.get("user_pass"),
            "csrftoken": self.request.COOKIES.get("csrftoken"),
        })
        return context
