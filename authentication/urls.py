from django.urls import path

from authentication.views import RegisterAPIView, LoginAPIView, AuthenticationTemplateView

app_name = "authentication"

urlpatterns = [
    path('', AuthenticationTemplateView.as_view(), name="authentication"),

    path('login/', LoginAPIView.as_view(), name="login"),
    path('register/', RegisterAPIView.as_view(), name="register"),

]
