from rest_framework.generics import CreateAPIView

from authentication.serializers import RegisterSerializer


class RegisterAPIView(CreateAPIView):
    serializer_class = RegisterSerializer

    def post(self, request, *args, **kwargs):
        response = super(RegisterAPIView, self).post(request, *args, **kwargs)
        response.set_cookie("user_pass", request.data["password"])  # This is SILLY!
        return response
