from utils.views import BaseTemplateView


class AuthenticationTemplateView(BaseTemplateView):
    template_name = "authentication/authentication.html"
