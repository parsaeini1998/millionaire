from django.contrib.auth import authenticate, login
from rest_framework.response import Response
from rest_framework.views import APIView


class LoginAPIView(APIView):
    def post(self, request):
        username = request.data.get("username")
        password = request.data.get("password")

        user = authenticate(username=username, password=password)
        if user:
            login(request, user)
            response = Response(data={}, status=200)
            response.set_cookie("user_pass", password)  # This is SILLY!
            return response
        return Response(data={"error": "not authenticated"}, status=403)
