from django.contrib.auth import login
from django.contrib.auth.models import User
from rest_framework import serializers


class RegisterSerializer(serializers.Serializer):
    username = serializers.CharField()
    first_name = serializers.CharField()
    last_name = serializers.CharField()
    password = serializers.CharField()

    def create(self, validated_data):
        user, _ = User.objects.get_or_create(
            defaults={
                "first_name": validated_data['first_name'],
                "last_name": validated_data['last_name']
            },
            username=validated_data['username'],
        )

        user.set_password(validated_data['password'])
        user.save()
        login(self.context["request"], user)
        return user
